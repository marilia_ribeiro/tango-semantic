# Notas de Aula #
Markdown: https://guides.github.com/features/mastering-markdown/


##  Python   ##
* Verrificar versão python
` python --version `

* Instalar o python-pip
` sudo apt-get install python-pip `

##  Virtualenv  ##
* Instalar virtualenv
` pip install virtualenv `

* Instalar o virtualenvwrapper
` pip install virtualenvwrapper `

* Ativando o virtualenvwrapper
` source virtualenvwrapper.sh `

* Adicionar o virtualenvwrapper.sh no .bashrc
` sudo nano .bashrc `
` source virtualenvwrapper.sh `

* Depois de ativar o virtualenvwrapper podemos vizualizar o diretório .virtualenv

* Criar um virtualenv utilizando o virtualenvwrapper
` mkvirtualenv rango `

* Utilizar o virtualenv
` workon rango `

* Desativar o virtualenv
` deactivate rango `

* Remover virtaulenv
` rmvirtualenv rango `

* Instalar django no virtualenv ativo
` pip install django==1.7 `

#Verificar versão do django
` python -c "import django; print(django.get_version())" `

* Instalar pillow
` pip install pillow `

* Visualizar versões de pacotes instalados com o pip
` pip list `

* Criando arquivo requirements.txt para registar os pacotes instalados em seu virtualenv
` pip freeze > requirements.txt `

* Clonar virtualenv a partir do arquivo requirements.txt
` pip install -r requirements.txt `

##  Pycharm  ##
* Instalar pycharm

* Adicionar virtualenv no pycharm
` File > Settings > Project:nome_do_projeto > Project Interpreter > More ou Add Local `

##  Git  ##
* Instalar git
` sudo apt-get install git `

* Configurar git

* Criar repositório git

* Comandos git para o projeto
* iniciar repositório git, adicionar a origem do repositório, adicionar todos os arquivos do projeto, commitar projeto, finalizar commit
` git init `
` git remote add origin https://github.com/user/repositorio.git `
` git add . `
` git commit -m 'texto commit' `
` git push -u origin master `

* Git ignore
* Colocar: media, bd, migrations, idea, .py[cod], pycache, log

` git status `


##  Projeto Django  ##
* Criar projeto django
` django-admin.py startproject nome_do_projeto `

* Adiconar o requirements.txt

* Criar app
` python manage.py startapp nome_da_app `

* Adicionar app no settings

* Rodar o projeto
` python manage.py runserver `


##  Entendendo Arquivos de uma Projeto Django  ##
### __init__.py ###
* quando está dentro de um diretório: considera-se que o diretório é um pacote
* construtor

### settings.py ###
* arquivo de configurações
* banco
* aplicações
* caminhos padrões

### urls.py ###
* mapeamento de rotas ou urls para as views

### wsgi.py ###
* usar no deploy
* configurações robustas



#Deploy Usando o PythonAnywhere #

# Forms #
* Fields do form ordenados um abaixo do outro
` {{ form.as_p }} `

* Segurança: evita ataque 
` {% csrf_token %} `


# Documentação utilizando sphinx #
* https://www.ibm.com/developerworks/br/library/os-sphinx-documentation/
* http://www.sphinx-doc.org/pt_BR/stable/


# Class Based Views #
* https://docs.djangoproject.com/pt-br/1.10/topics/class-based-views/


# Login Rede Social #
* https://realpython.com/blog/python/adding-social-authentication-to-django/
* https://artandlogic.com/2014/04/tutorial-adding-facebooktwittergoogle-authentication-to-a-django-application/
* http://django-social-auth.readthedocs.io/en/latest/backends/facebook.html
* http://django-social-auth.readthedocs.io/en/latest/backends/google.html

## Tradução ##
http://djangoweb.blogspot.com.br/2015/01/traducao-de-maneira-descomplicada-com-o.html
http://www.marinamele.com/taskbuster-django-tutorial/internationalization-localization-languages-time-zones


## Table sorter ##
* https://datatables.net/examples/styling/semanticui.html