$(document).ready(function(){

    // --- about-btn --- //

    // add alert ao clicar no botão
    $("#about-btn").click(function(envet) {
        alert("You clicked the button using JQuery!");
    });

    // add uma classe do bootstrap ao botão para ficar com um estilo diferente
    $("#about-btn").addClass('btn btn-primary');

    // cada vez que der um click faz um append da letra "o" ao texto "Helo" que está em uma div
    $("#about-btn").click( function(event) {
    msgstr = $("#msg").html()
        msgstr = msgstr + "o"
        $("#msg").html(msgstr)
     });


    // --- tag <p></p> --- //

    // deixa todas as tags <p></p> com cor vermelha ao passar o mouse em cima e azul como cor padrão
    $("p").hover( function() {
            $(this).css('color', 'red');
    },
    function() {
            $(this).css('color', 'blue');
    });


});
