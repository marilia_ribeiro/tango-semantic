# -*- coding: utf-8 -*-
from django import forms
from rango.models import Page

class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128, help_text="Title page:")
    url = forms.URLField(max_length=200, help_text="URL:")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    # img = forms.ImageField(upload_to='page_images', blank=True)


    class Meta:
        #associacao entre ModelForm
        model = Page

        # quais campos quero colocar no forms
        # esses campos precisam estar presentes no models
        # campos NULL ??
        # esconder fks
        # excluir o campo category do forms
        exclude = ('category',) #exclude = ('category', 'img')
        # ou especificar os campos para incluir
        # field = ('title', 'url', 'views')

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        # se a url não está vazia e não inicia com http
        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['url'] = url
            return cleaned_data