from django import forms
from rango.models import Category

class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=128, help_text="Category:")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    # img = forms.ImageField(upload_to='category_images', blank=True)

    #informacoes adicionais a classe
    class Meta:
        #associacao entre ModelForm
        model = Category
        fields = ('name', 'img')
