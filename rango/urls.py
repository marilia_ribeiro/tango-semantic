from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from rango import views
#from . import views
#from rango import views

from django.conf import settings
from django.conf.urls.static import static

from rango.views import Index

urlpatterns = patterns('',
    # url(r'^$', views.index, name='index'),
    url(r'^$',Index.as_view(), name='index'),
    url(r'^about/$', TemplateView.as_view(template_name='rango/about.html'), name='about'),
    url(r'^restricted/$', views.restricted, name='restricted'),
    url(r'^search/$', views.search, name='search'),

    #category
    url(r'^category/(?P<category_name_slug>[\w\-]+)/$', views.category, name='category'),
    url(r'^add_category/$', views.add_category, name='add_category'),
    url(r'^view_categories/$', views.view_categories, name='view_categories'),
    url(r'^category/delete_category/(?P<pk>\d+)$', views.delete_category, name='delete_category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/detail_category/$', views.detail_category, name='detail_category'),
    url(r'^edit_category/(?P<id>\d+)/$', views.edit_category, name='edit_category'),
    url(r'^like_category/$', views.like_category, name='like_category'),
    url(r'^unlike_category/$', views.unlike_category, name='unlike_category'),
    url(r'^suggest_category/$', views.suggest_category, name='suggest_category'),

    #page
    url(r'^category/(?P<category_name_slug>[\w\-]+)/add_page/$', views.add_page, name='add_page'),
    url(r'^add_page/$', views.add_page_uncategorized, name='add_page_uncategorized'),
    url(r'^view_pages/$', views.view_pages, name='view_pages'),
    url(r'^page/delete_page/(?P<pk>[\w\-]+)$', views.delete_page, name='delete_page'),
    url(r'page/(?P<pk>[\w\-]+)/detail_page/$', views.detail_page, name='detail_page'),
    url(r'^page/edit_page/(?P<id>\d+)/$', views.edit_page, name='edit_page'),
    url(r'^goto/$', views.track_url, name='goto'),

    #django-registrations-redux
    url(r'^$', TemplateView.as_view(template_name='registration/login.html'), name='login'),
    url(r'^$', login_required(TemplateView.as_view(template_name='index.html'), login_url='/'), name='home'),
    url(r'^$', views.logout, name='logout'),
    # url(r'^register/$', UserProfile.as_view(), name='register'),
    url(r'^register/$', views.register, name='register'),
    url(r'^profile$', views.profile, name='profile')

)

if not settings.DEBUG:
      urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
      urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

