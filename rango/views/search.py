#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from rango.functions import run_query


def search(request):
    '''Função search do bing.'''
    result_list = []

    if request.method == 'POST':
        query = request.POST['query'].strip()

        if query:
            # Run our Bing function to get the results list!
            result_list = run_query(query)

    return render(request, 'rango/search.html', {'result_list': result_list, 'query':query})