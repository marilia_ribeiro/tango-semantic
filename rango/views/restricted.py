#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

@login_required
def restricted(request):
    # testando se os cookies estão funcionando
    if request.session.test_cookie_worked():
        print ">>>> TEST COOKIE WORKED!"
        request.session.delete_test_cookie()


    message = "Since you're logged in, you can see  this exit!"
    return HttpResponse(message)