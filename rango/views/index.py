#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from rango.models import Category
from rango.models import Page
from datetime import datetime
from django.views.generic import ListView


class Index(ListView):
    '''Retorna um dicionário contendo as 5 categorias mais curtidas e as páginas mais visitadas.
    Além disso utiliza cookies para adicionar um contador de visitas na página index.html'''

    # model = Category
    template_name = 'rango/index.html'
    context_object_name = 'categories'

    def get_queryset(self):
        category_list = Category.objects.order_by('-likes')[:5]
        return category_list

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        page_list = Page.objects.order_by('-views')[:5]  # páginas mais visitadas
        context['pages'] =  page_list

        visits = self.request.session.get('visits')
        if not visits:
            visits = 1
        reset_last_visit_time = False

        last_visit = self.request.session.get('last_visit')
        if last_visit:
            last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

            if (datetime.now() - last_visit_time).days > 0:
                visits += 1
                reset_last_visit_time = True
        else:
            reset_last_visit_time = True

        if reset_last_visit_time:
            self.request.session['last_visit'] = str(datetime.now())
            self.request.session['visits'] = visits

        context['visits'] = visits
        context['last_visit'] = last_visit

        return context

def index(request):
    '''Retorna um dicionário contendo as 5 categorias mais curtidas e as páginas mais visitadas.
    Além disso utiliza cookies para adicionar um contador de visitas na página index.html'''
    
    # cookies
    #request.session.set_test_cookie()

    category_list = Category.objects.order_by('-likes')[:5] #categorias mais curtidas
    page_list = Page.objects.order_by('-views')[:5]  # páginas mais visitadas

    context_dict = {'categories': category_list, 'pages': page_list}

    #cookie para controlar número de visitas no site
    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).days > 0:
            visits += 1
            reset_last_visit_time = True
    else:
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits

    context_dict['visits'] = visits
    context_dict['last_visit'] = last_visit
    response = render(request, 'rango/index.html', context_dict)

#    return render(request, 'rango/index.html', context_dict)
    return response
