# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from rango.models import Category,Page
from rango.forms import PageForm
from rango.views.index import index


@login_required
def add_page(request, category_name_slug):
    '''Função para inserir uma página no banco de dados.
     Associa a página com uma categoria através do category_name_slug.'''

    try:
        cat = Category.objects.get(slug=category_name_slug)

    except Category.DoesNotExist:
        cat = None

    #TODO: Verificar se página já existe e não permitir adicionar novamente

    if request.method == 'POST':
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                if 'picture' in request.FILES:
                    page.img = request.FILES['img']
                page.save()

                # return category(request, category_name_slug)
                return redirect('/rango/category/'+category_name_slug +'/')
        else:
            print(form.errors)
    else:
        form = PageForm()

    context_dict =  {'form':form, 'category':cat}
    return render(request, 'rango/add_page.html', context_dict)


@login_required
def add_page_uncategorized(request):
    '''Função para inserir uma página no banco de dados que não possui relação direta a uma categoria.'''

    cat = Category.objects.all()

    #TODO: Verificar se página já existe e não permitir adicionar novamente

    if request.method == 'POST':
        form = PageForm(request.POST)
        selected_option = request.POST.get('category_options', None)
        category = Category.objects.get(name=selected_option)

        if form.is_valid():
            if selected_option:
                # print(category.name, category.id, category.slug)
                page = form.save(commit=False)
                page.category = category
                page.views = 0
                page.save()
                return redirect('/rango/category/' + category.slug + '/')
        else:
            print(form.errors)
    else:
        form = PageForm()

    context_dict =  {'form':form, 'categories':cat}
    return render(request, 'rango/add_page.html', context_dict)


@login_required
def add_page(request, category_name_slug):
    '''Função para inserir uma página no banco de dados.
     Associa a página com uma categoria através do category_name_slug.'''

    try:
        cat = Category.objects.get(slug=category_name_slug)

    except Category.DoesNotExist:
        cat = None

    #TODO: Verificar se página já existe e não permitir adicionar novamente

    if request.method == 'POST':
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()

                # return category(request, category_name_slug)
                return redirect('/rango/category/'+category_name_slug +'/')
        else:
            print(form.errors)
    else:
        form = PageForm()

    context_dict =  {'form':form, 'category':cat}
    return render(request, 'rango/add_page.html', context_dict)


def track_url(request):
    '''Função que adiciona visualizações a páginas.'''
    page_id = None
    url = '/rango/'

    #TODO verificar se realmente está entrando no get
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                url = page.url
                page.save()
            except:
                pass
    return redirect(url)

def view_pages(request):
    '''Retorna uma lista com todas a páginas.'''
    pages = Page.objects.all()
    return render(request, 'rango/view_pages.html', {'pages': pages})

def delete_page(request, pk):
    '''Função de exclusão de uma página'''

    page = Page.objects.get(pk=pk)
    if request.method == 'POST':
        page.delete()
        return index(request)

    return render(request, 'rango/confirm_delete_page.html', {'page': page})

def edit_page(request, id):
    '''Edição da página selecionada'''
    if request.method == 'POST':
        if id:
            page = Page.objects.get(id=id)
            form = PageForm(request.POST, instance=page)
        else:
            form = PageForm(request.POST)

        if form.is_valid():
            form.save()
            return index(request)

    else:
        if id:
            page = Page.objects.get(id=id)
            form = PageForm(instance=page)

    return render(request, 'rango/edit_page.html', {'form':form, 'page':page})

def detail_page(request, pk):
    '''Mostra detalhes da página'''
    page = Page.objects.get(pk=pk)
    return render(request, 'rango/edit_page.html', {'page': page})