# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import render

from rango.functions import get_category_list, run_query
from rango.models import Category
from rango.models import Page
from rango.forms import CategoryForm
from rango.views import index
from django.contrib.auth.decorators import login_required
from django.views.generic import View, TemplateView
from django.shortcuts import redirect

# class Category(TemplateView):
#     template_name = 'rango/category.html'
#     category_name_slug = ''
#
#     def get_context_data(self, **kwargs):


def category(request, category_name_slug):
    '''Retorna um dicionário contendo o resultado da pesquisa utilizando o bing
    e as páginas ordenadas pela quantidade de visualizações'''

    context_dict = {}
    context_dict['result_list'] = None
    context_dict['query'] = None
    if request.method == 'POST':
        query = request.POST['query'].strip()

        if query:
            # Run our Bing function to get the results list!
            result_list = run_query(query)

            context_dict['result_list'] = result_list
            context_dict['query'] = query

    try:
        category = Category.objects.get(slug=category_name_slug)
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages
        context_dict['category'] = category
    except Category.DoesNotExist:
        pass

    if not context_dict['query']:
        context_dict['query'] = category.name

    return render(request, 'rango/category.html', context_dict)

@login_required
def add_category(request):
    '''Função de cadastro de categorias'''

    # um HTTP post?
    if request.method == 'POST':
        form = CategoryForm(request.POST)

        # forms é valido
        if form.is_valid():
            #salvar a nova categoria no banco
            if 'img' in request.FILES:
                form.img = request.FILES['img']
            form.save(commit=True)

            #redirecionando para a página inicial index() view
            return index(request)
        else:
            # printar o erro no terminal
            print(form.errors)

    else:
        # se o metodo não for POST, mostrar detalhes do form
        form = CategoryForm()

    return render(request, 'rango/add_category.html', {'form': form})

@login_required
def like_category(request):
    '''Função utilizada para adicionar likes à uma categoria'''

    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET['category_id']

    likes = 0
    if cat_id:
        cat = Category.objects.get(id=int(cat_id))
        if cat:
            likes = cat.likes + 1
            cat.likes = likes
            cat.save()

    return HttpResponse(likes)

@login_required
def unlike_category(request):
    '''Função utilizada para adicionar unlikes à uma categoria'''

    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET['category_id']

    likes = 0
    if cat_id:
        cat = Category.objects.get(id=int(cat_id))
        if cat:
            likes = cat.likes - 1
            cat.likes = likes
            cat.save()

    return HttpResponse(likes)

def suggest_category(request):
    '''Adiciona sugestões a uma categoria'''

    cat_list = []
    starts_with = ''
    if request.method == 'GET':
            starts_with = request.GET['suggestion']

    cat_list = get_category_list(8, starts_with)

    return render(request, 'rango/cats.html', {'cat_list': cat_list })

def view_categories(request):
    '''Retorna uma lista contendo todas as categorias'''

    categories = Category.objects.all()
    return render(request, 'rango/view_categories.html', {'categories': categories})

def delete_category(request, pk):
    '''Função de exclusão de uma categoria'''

    category = Category.objects.get(pk=pk)
    pages = Page.objects.filter(category=category).all()

    if request.method == 'POST':
        pages.delete()
        category.delete()
        return index(request)

    return render(request, 'rango/confirm_delete_category.html', {'category': category, 'pages': pages})

def edit_category(request, id):
    '''Edição da categoria selecionada'''

    if request.method == 'POST':
        if id:
            category = Category.objects.get(id=id)

            form = CategoryForm(request.POST,instance=category)
        else:
            form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return index(request)
    else:
        if id:
            category = Category.objects.get(id=id)
            form = CategoryForm(instance=category)

    return render(request, 'rango/edit_category.html', {'form':form, 'category': category})


def detail_category(request, category_name_slug):
    '''Mostra detalhes da categoria'''
    category = Category.objects.get(slug=category_name_slug)
    return render(request, 'rango/edit_category.html', {'category': category})