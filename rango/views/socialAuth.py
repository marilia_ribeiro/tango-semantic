#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from django.contrib.auth import logout as auth_logout

def logout(request):
    '''Função de logout utilizando django-registration-redux.'''
    auth_logout(request)
    return redirect('/')
