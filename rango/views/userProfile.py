# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django import forms
from django.contrib.auth.models import User
from rango.forms import UserForm, UserProfileForm
from rango.models import Category, Page, UserProfile
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


def register(request):
    '''Função de cadastro de usuários.'''
    #return HttpResponse('register')
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()

            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            registered = True

        # else:
        #     print user_form.errors, profile_form.erros

    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request, 'rango/register.html', {'user_form':user_form, 'profile_form':profile_form, 'registered':registered})
    # return render(request, 'registration/registration_form.html', {'user_form':user_form, 'profile_form':profile_form, 'registered':registered})

def user_login(request):
    '''Função de login.'''

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse('Your Rango account is disabled.')

        else:
            print 'Invalid login details: {0}, {1}'.format(username,password)
            return HttpResponse('Invalid login details supplied.')
    else:
        return render(request, 'registration/login.html', {})


    return request(request, 'registration/login.html', {'login':'login'})


@login_required
def user_logout(request):
    '''Função de logout.'''

    logout(request)
    return HttpResponseRedirect('/rango/')

@login_required()
def profile(request):
    profiles = UserProfile.objects.all()
    return render(request, 'registration/profile.html', {'profiles': profiles})
