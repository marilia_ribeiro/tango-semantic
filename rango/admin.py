from django.contrib import admin
from rango.models import Category, Page, UserProfile
from modeltranslation.admin import TranslationAdmin

class CategoryAdmin(admin.ModelAdmin):
# class CategoryAdmin(TranslationAdmin):
    prepopulated_fields = {'slug': ('name', )}

class PageAdmin(admin.ModelAdmin):
# class PageAdmin(TranslationAdmin):
    list_display = ('category', 'title', 'url') #sequencia das colunas no admin
    ordering = ('views',) #ordenas por likes

#admin.site.register(Category)
admin.site.register(Category, CategoryAdmin)
#admin.site.register(Page, PageAdmin)
admin.site.register(Page)
admin.site.register(UserProfile)
