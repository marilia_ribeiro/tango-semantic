from django.db import models
from category import Category
from django.utils.translation import ugettext_lazy as _

class Page(models.Model):
    '''Model page'''
    category = models.ForeignKey(Category)
    title  = models.CharField(max_length=128)
    url = models.URLField()
    views = models.IntegerField(default=0)
    img = models.ImageField(upload_to='page_images', blank=True)

    def __unicode__(self):
        return self.title