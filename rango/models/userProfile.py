# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from tango_with_django_project.settings import MEDIA_URL


class UserProfile(models.Model):
    '''Model UserProfile'''
    #campo obrigatório - relaciona com model User do django
    user = models.OneToOneField(User)

    #campos adicionais
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)
    # picture = models.ImageField(upload_to=MEDIA_URL + 'profile_images', blank=True)

    def __unicode__(self):
        return self.user.username