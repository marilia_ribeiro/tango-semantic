from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

class Category(models.Model):
    '''Model category'''
    name = models.CharField(max_length=128, unique=True)
    views = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    slug = models.SlugField() #normaliza url - retira espacos, acentos e padroniza a url - url amigavel
    img = models.ImageField(upload_to='category_images', blank=True)

    # grava  no bd
    def save(self, *args, **kwargs):
        '''views nunca pode ser menor que 0 ---> test.py'''
        if self.views < 0:
           self.views = 0
        self.slug = slugify(self.name) #toda vez que salvar a categoria redefine a url
        super(Category, self).save(*args, **kwargs)

    # o que quero retornar num print ou em uma funcao
    def __unicode__(self): #Py 2 = __unicode__; Py 3 __str__
        return self.name