from django.contrib import admin
from rango.models import Category, Page, UserProfile
from modeltranslation.translator import translator, TranslationOptions
from .models import *

# class CategoryOptions(TranslationOptions):
#     fields = ('name')
# translator.register(Category, CategoryOptions)
#
# class PageOptions(TranslationOptions):
#     fields = ('category', 'title')
# translator.register(Category, CategoryOptions)