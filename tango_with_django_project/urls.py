from django.conf.urls import patterns, include, url
from django.contrib import admin
#admin.autodiscover()
from django.conf import settings
from registration.backends.simple.views import RegistrationView
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

class MyRegistrationView(RegistrationView):
    def get_sucess_url(self, request, user):
        return '/rango/'

urlpatterns = i18n_patterns('',
    ("^admin/", include(admin.site.urls)),
)

urlpatterns += i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^rango/', include('rango.urls')),
    url(r'^accounts/register/$', MyRegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.backends.simple.urls')),

    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
