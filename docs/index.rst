.. rango documentation master file, created by
   sphinx-quickstart on Wed Nov 30 22:03:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rango's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   modules/models
   modules/views


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

